#!/bin/sh

DOTFILES=~/dotfiles
BACKUP=~/dotfiles-$(date '+%Y%m%d-%H%M')
FILES='vimrc zshrc'

cd $HOME
mkdir $BACKUP
for F in $FILES; do
	mv ~/.$F $BACKUP
	ln -s $DOTFILES/$F ~/.$F
done
